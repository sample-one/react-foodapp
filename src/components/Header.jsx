import React,{useState} from 'react'
import { home, logo, user,search,cart,map,menu } from '../assets'

function Header() {
  
  let [ menuBar, setMenuBar] = useState("")


  function handleMenuBar(){
     
  setMenuBar(prevState =>(prevState === ""? 'active' :""))

  }


  return (
    <div>

<header>
        <div className="container">
            <div className="logo">
                <img src={logo} alt="" />
            </div>
            <ul>
                <li><a href="#"><img src={home} />Home</a></li>
                <li><a href="#"><img src={search} />Search</a></li>
                <li><a href="#"><img src={cart} />Cart</a></li>
                <li><a href="#"><img src={user}/> Account</a></li>
            </ul>

            <div className="location">
                <span><img src={map}/><b>Tirur, Malappuram</b></span>
            </div>
               
              {/* Toggle bar */}
            <div className={`toggle-bar ${menuBar}`}>
                <ol>
                <li><a href="#"><img src={home} />Home</a></li>
                <li><a href="#"><img src={search} />Search</a></li>
                <li><a href="#"><img src={cart} />Cart</a></li>
                <li><a href="#"><img src={user}/> Account</a></li>
                </ol>
                <span><img src={map}/><b>Tirur, Malappuram</b></span>
            </div>
             
              <div className="toggle-menu">
                <img src={menu} onClick={handleMenuBar} />
              </div>
             
        </div>
      </header>
    </div>
  )
}

export default Header