// hero images
import slide1 from "./slide1.png"
import slide2 from "./slide2.png"
import slide3 from "./slide3.png"

import logo from './logo.png'
import home from "./home.png"
import search from "./search.png"
import cart from "./cart.png"
import user from "./user.png"

import map from "./map.png"
import menu from "./menu.png"
export  {
  slide1,
  slide2,
  slide3,
  logo,
  home,
  search,
  cart,
  user,
  map,
  menu,
}